import 'package:flutter/material.dart';

class BodyMassIndexTab extends StatefulWidget {
  const BodyMassIndexTab({Key? key}) : super(key: key);

  @override
  _BodyMassIndexTabState createState() => _BodyMassIndexTabState();
}

class _BodyMassIndexTabState extends State<BodyMassIndexTab> {
  final TextEditingController _weightController = TextEditingController();
  final TextEditingController _heightController = TextEditingController();
  bool isMetric = true; // Track whether metric or imperial units are selected
  String weightUnit = 'kg';
  String heightUnit = 'cm';
  double bmi = 0.0;

  List<String> underweightTips = [
    'Increase your calorie intake to gain weight in a healthy way.',
    'Include protein-rich foods like lean meats, eggs, and legumes in your diet.',
    'Consult a nutritionist to create a balanced meal plan.',
  ];

  List<String> normalWeightTips = [
    'Maintain your current weight with a balanced diet and regular exercise.',
    'Eat a variety of fruits, vegetables, whole grains, and lean proteins.',
    'Stay physically active for at least 30 minutes every day.',
  ];

  List<String> overweightTips = [
    'Create a calorie deficit by reducing portion sizes and choosing healthier food options.',
    'Increase your physical activity level to burn more calories.',
    'Consult a healthcare professional for personalized weight loss advice.',
  ];

  List<String> obeseTips = [
    'Seek professional help to develop a comprehensive weight loss plan.',
    'Focus on making long-term lifestyle changes rather than quick fixes.',
    'Incorporate regular exercise and healthy eating habits into your routine.',
  ];

  String bmiText = '';
  Color resultColor = Colors.black;
  List<String> tips = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(height: 16),
            const Text(
              'Calculate BMI',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 16),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _weightController,
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    decoration: InputDecoration(
                      labelText: 'Weight ($weightUnit)',
                    ),
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: TextField(
                    controller: _heightController,
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    decoration: InputDecoration(
                      labelText: 'Height ($heightUnit)',
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Metric'),
                Switch(
                  value: isMetric,
                  onChanged: (value) {
                    setState(() {
                      isMetric = value;
                      if (isMetric) {
                        weightUnit = 'kg';
                        heightUnit = 'cm';
                      } else {
                        weightUnit = 'lbs';
                        heightUnit = 'in';
                      }
                    });
                  },
                ),
                Text('Imperial'),
              ],
            ),
            const SizedBox(height: 16),
            ElevatedButton(
              onPressed: _calculateBMI,
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 55),
                backgroundColor: Theme.of(context).colorScheme.secondary,
                textStyle: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              child: const Text('Calculate'),
            ),
            const SizedBox(height: 32),
            bmi!= 0
               ? Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'Your BMI: ${bmi.toStringAsFixed(1)}',
                        style: TextStyle(color: resultColor),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        'BMI Category: $bmiText',
                        style: TextStyle(color: resultColor),
                      ),
                      const SizedBox(height: 8),
                      Container(
                        margin: const EdgeInsets.only(bottom: 8),
                        child: LinearProgressIndicator(
                          value: bmi / 50,
                          backgroundColor: Colors.grey[200],
                          valueColor: AlwaysStoppedAnimation(resultColor),
                        ),
                      ),
                      Text(
                        'Tips:',
                        style: TextStyle(color: resultColor),
                      ),
                      const SizedBox(height: 4),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:tips
                            .map((tip) => Text(
                                  '- $tip',
                                  style: TextStyle(color: resultColor),
                                ))
                            .toList(),
                      ),
                    ],
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  void _calculateBMI() {
    final weight = double.tryParse(_weightController.text) ?? 0.0;
    final height = double.tryParse(_heightController.text) ?? 0.0;

    if (weight <= 0 || height <= 0) {
      setState(() {
        bmi = 0;
        bmiText = '';
        tips = [];
        resultColor = Colors.black;
      });
      return;
    }

    if (isMetric) {
      bmi = weight / ((height / 100) * (height / 100));
    } else {
      bmi = (weight / (height * height)) * 703; // Conversion from lbs/in^2 to BMI
    }

    setState(() {
      if (bmi < 18.5) {
        bmiText = 'Underweight';
        resultColor = Colors.red;
        tips = underweightTips;
      } else if (bmi >= 18.5 && bmi < 24.9) {
        bmiText = 'Normal weight';
        resultColor = Colors.green;
        tips = normalWeightTips;
      } else if (bmi >= 25 && bmi < 29.9) {
        bmiText = 'Overweight';
        resultColor = Colors.orange;
        tips = overweightTips;
      } else {
        bmiText = 'Obese';
        resultColor = Colors.red;
        tips = obeseTips;
      }

      _weightController.clear();
      _heightController.clear();
    });
  }
}
