import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late SharedPreferences _prefs;
  String _selectedDifficulty = 'Difficulty Level';

  @override
  void initState() {
    super.initState();
    _loadPreferences();
  }

  void _loadPreferences() async {
    _prefs = await SharedPreferences.getInstance();
    setState(() {
      _selectedDifficulty = _prefs.getString('difficulty') ?? 'Difficulty Level';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            ExpansionTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Difficulty Level',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              children: [
                ListTile(
                  title: Text('Easy'),
                  tileColor: _selectedDifficulty == 'Easy' ? Colors.purple.withOpacity(0.1) : null,
                  trailing: _selectedDifficulty == 'Easy' ? Icon(Icons.check, color: Colors.purple) : null,
                  onTap: () {
                    _updateDifficulty('Easy', 0.045, 4.12);
                  },
                ),
                ListTile(
                  title: Text('Medium'),
                  tileColor: _selectedDifficulty == 'Medium' ? Colors.purple.withOpacity(0.1) : null,
                  trailing: _selectedDifficulty == 'Medium' ? Icon(Icons.check, color: Colors.purple) : null,
                  onTap: () {
                    _updateDifficulty('Medium', 0.13, 60);
                  },
                ),
                ListTile(
                  title: Text('Hard'),
                  tileColor: _selectedDifficulty == 'Hard' ? Colors.purple.withOpacity(0.1) : null,
                  trailing: _selectedDifficulty == 'Hard' ? Icon(Icons.check, color: Colors.purple) : null,
                  onTap: () {
                    _updateDifficulty('Hard', 0.15, 70);
                  },
                ),
              ],
            ),
            // Add other settings options here
          ],
        ),
      ),
    );
  }

  void _updateDifficulty(String difficulty, double stepFactor, double peakThreshold) {
    setState(() {
      _selectedDifficulty = difficulty;
    });
    _prefs.setString('difficulty', difficulty);
    _prefs.setDouble('stepFactor', stepFactor);
    _prefs.setDouble('peakThreshold', peakThreshold);
    Navigator.pop(context);
  }
}

