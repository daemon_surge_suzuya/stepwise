import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StepGraph extends StatelessWidget {
  final List<_DailySteps> data;

  const StepGraph({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300, // Set an appropriate height
      child: SfCartesianChart(
        primaryXAxis: CategoryAxis(),
        primaryYAxis: NumericAxis(minimum: 0),
        title: ChartTitle(text: 'Daily Steps Analysis'), // Removed 'const'
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<dynamic, dynamic>>[ // Changed the series type
          LineSeries<_DailySteps, String>(
            dataSource: data,
            xValueMapper: (_DailySteps steps, _) => steps.date.toString(),
            yValueMapper: (_DailySteps steps, _) => steps.steps,
            name: 'Steps',
            dataLabelSettings: const DataLabelSettings(isVisible: true),
          ),
        ],
      ),
    );
  }
}

class _DailySteps {
  final DateTime date;
  int steps;

  _DailySteps({required this.date, this.steps = 0});
}

