import 'package:flutter/material.dart';
import 'TabbedScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const TabbedScreen());
}

