import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'BodyMassIndex.dart';
import 'Pedometer.dart';
// import 'StepGraph.dart';

class TabbedScreen extends StatefulWidget {
  const TabbedScreen({Key? key}) : super(key: key);

  @override
  _TabbedScreenState createState() => _TabbedScreenState();
}

class _TabbedScreenState extends State<TabbedScreen> {
  int _currentIndex = 0;
  final List<Widget> _tabs = [
    const PedometerScreen(),
    const BodyMassIndexTab(key: Key('bmiTab')),
    // StepGraph(data: []),
  ];
  final List<String> _tabNames = [
    'Pedometer',
    'Body Mass Index',
    // 'Step Graph', // New tab name
  ];
  final List<IconData> _tabIcons = [
    Icons.home,
    Icons.calculate,
    // Icons.directions_walk,
  ];
  final Color _tabTextColor = const Color(0xFF65FFDB); // Hex color #65ffdb

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            _tabNames[_currentIndex],
            style: TextStyle(
              color: _tabTextColor, // Set text color to the specified hex color
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          centerTitle: true,
          backgroundColor: Color(0xFF303030), // Set app bar color to black
        ),
        body: PageTransitionSwitcher(
          duration: const Duration(milliseconds: 500),
          transitionBuilder: (
            Widget child,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) {
            return FadeThroughTransition(
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          },
          child: _tabs[_currentIndex],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            for (var i = 0; i < _tabs.length; i++)
              BottomNavigationBarItem(
                icon: Icon(_tabIcons[i]),
                label: _tabNames[i],
              ),
          ],
        ),
      ),
      // Set the background color of the entire app to black
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
    );
  }
}

