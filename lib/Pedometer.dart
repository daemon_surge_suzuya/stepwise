import 'dart:async';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PedometerScreen extends StatefulWidget {
  const PedometerScreen({Key? key}) : super(key: key);

  @override
  _PedometerScreenState createState() => _PedometerScreenState();
}

class _PedometerScreenState extends State<PedometerScreen> {
  late SharedPreferences _prefs;
  int _steps = 0;
  int _goal = 10000;
  double _caloriesBurned = 0.0;
  double _distanceCovered = 0.0;
  Duration _timeSpentWalking = Duration.zero;
  final double _stepFactor = 0.11;
  final double _peakThreshold = 50;
  List<double> _accelerationValues = [];
  int _tapCount = 0;
  late StreamSubscription<AccelerometerEvent> _streamSubscription;

  @override
  void initState() {
    super.initState();
    _loadData();
    startListening();
  }

  @override
  void dispose() {
    _streamSubscription.cancel();
    super.dispose();
  }

  void _loadData() async {
    _prefs = await SharedPreferences.getInstance();
    setState(() {
      _goal = _prefs.getInt('goal') ?? 10000;
      _steps = _prefs.getInt('steps') ?? 0;
      _caloriesBurned = _prefs.getDouble('caloriesBurned') ?? 0.0;
      _distanceCovered = _prefs.getDouble('distanceCovered') ?? 0.0;
      int timeInSeconds = _prefs.getInt('timeSpentWalking') ?? 0;
      _timeSpentWalking = Duration(seconds: timeInSeconds);
    });
  }

  void _saveData() {
    _prefs.setInt('goal', _goal);
    _prefs.setInt('steps', _steps);
    _prefs.setDouble('caloriesBurned', _caloriesBurned);
    _prefs.setDouble('distanceCovered', _distanceCovered);
    _prefs.setInt('timeSpentWalking', _timeSpentWalking.inSeconds);
  }

  void startListening() {
    _streamSubscription = accelerometerEvents.listen((AccelerometerEvent event) {
      final acceleration = event.x + event.y + event.z;
      _accelerationValues.add(acceleration);

      if (_accelerationValues.length >= 3) {
        final isPeak = checkForPeak();

        if (isPeak) {
          setState(() {
            _steps++;
            calculateCaloriesBurned();
            calculateDistance();
          });
        }
        _accelerationValues.removeAt(0);
      }
    });
  }

  bool checkForPeak() {
    final middleValue = _accelerationValues[1];
    if (middleValue > _accelerationValues[0] && middleValue > _accelerationValues[2]) {
      return middleValue > _peakThreshold;
    } else if (middleValue < _accelerationValues[0] && middleValue < _accelerationValues[2]) {
      return middleValue < -_peakThreshold;
    }
    return false;
  }

  void calculateCaloriesBurned() {
    _caloriesBurned = _steps * _stepFactor;
    _saveData();
  }

  void calculateDistance() {
    // Assuming a step is 0.7 meters on average
    double distanceInMeters = _steps * 0.7;
    _distanceCovered = distanceInMeters / 1000; // Convert to kilometers
    _saveData();
  }

  void resetData() {
    setState(() {
      _steps = 0;
      _caloriesBurned = 0.0;
      _distanceCovered = 0.0;
      _timeSpentWalking = Duration.zero;
    });
    _saveData();
  }

  void _setGoal(int newGoal) {
    setState(() {
      _goal = newGoal;
    });
    _saveData();
  }

  Future<void> _showResetDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Reset Steps'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Do you wish to reset the steps taken?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('No'),
            ),
            TextButton(
              onPressed: () {
                resetData();
                Navigator.of(context).pop();
              },
              child: const Text('Yes'),
            ),
          ],
        );
      },
    );
  }

@override
Widget build(BuildContext context) {
  double progress = (_steps / _goal).clamp(0.0, 1.0);

  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(height: 20),
        Expanded(
          child: Container(
            constraints: const BoxConstraints(maxHeight: 200.0),
            child: FractionallySizedBox(
              widthFactor: 0.9,
              heightFactor: 0.90,
              child: GestureDetector(
                onTap: () {
                  // Increment tap count on every tap
                  _tapCount++;
                  // If tap count reaches 3, reset it to 0 and show the reset dialog
                  if (_tapCount == 3) {
                    _tapCount = 0;
                    _showResetDialog(context);
                  }
                },
                child: CircularPercentIndicator(
                  radius: 100.0,
                  lineWidth: 11.0,
                  percent: progress,
                  circularStrokeCap: CircularStrokeCap.round,
                  center: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.directions_walk,
                        size: 34,
                        color: Color(0xFF65FFD8),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        '$_steps',
                        style: const TextStyle(
                          fontSize: 36,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _showGoalDialog(context);
                        },
                        child: Text(
                          'OF $_goal',
                          style: const TextStyle(
                            fontSize: 18,
                            color: Colors.grey,
                            decoration: TextDecoration.overline,
                          ),
                        ),
                      ),
                    ],
                  ),
                  progressColor: Color(0xFF65FFD8),
                  animation: true,
                  backgroundColor: Colors.white,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Icon(
                  Icons.location_on,
                  size: 36,
                  color: const Color(0xFF65FFD8),
                ),
                Text(
                  'Distance',
                  style: const TextStyle(fontSize: 18, color: Colors.black),
                ),
                Text(
                  '${_distanceCovered.toStringAsFixed(2)} km',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Icon(
                  Icons.local_fire_department,
                  size: 36,
                  color: const Color(0xFF65FFD8),
                ),
                Text(
                  'Calories',
                  style: const TextStyle(fontSize: 18, color: Colors.black),
                ),
                Text(
                  '${_caloriesBurned.toStringAsFixed(2)} kcal',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Icon(
                  Icons.timer,
                  size: 36,
                  color: const Color(0xFF65FFD8),
                ),
                Text(
                  'Time',
                  style: const TextStyle(fontSize: 18, color: Colors.black),
                ),
                Text(
                  '${_timeSpentWalking.inHours}:${(_timeSpentWalking.inMinutes % 60).toString().padLeft(2, '0')} h',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    ),
  );
}


  Future<void> _showGoalDialog(BuildContext context) async {
    TextEditingController controller = TextEditingController(text: _goal.toString());
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Set Goal'),
          content: TextField(
            controller: controller,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(labelText: 'Enter new goal'),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                int newGoal = int.tryParse(controller.text) ?? _goal;
                _setGoal(newGoal);
                Navigator.pop(context);
              },
              child: const Text('Set'),
            ),
          ],
        );
      },
    );
  }
}

