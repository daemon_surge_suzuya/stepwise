import 'package:flutter/material.dart';

class IconStats extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: const [
        IconWithText(
          icon: Icons.location_on,
          label: 'Distance',
          value: '5.2 km',
        ),
        IconWithText(
          icon: Icons.local_fire_department,
          label: 'Calories',
          value: '230 kcal',
        ),
        IconWithText(
          icon: Icons.timer,
          label: 'Time',
          value: '1h 30m',
        ),
      ],
    );
  }
}

class IconWithText extends StatelessWidget {
  final IconData icon;
  final String label;
  final String value;

  const IconWithText({
    Key? key,
    required this.icon,
    required this.label,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          icon,
          size: 36,
          color: const Color(0xFFD0EBFF),
        ),
        Text(
          label,
          style: const TextStyle(fontSize: 18, color: Colors.white),
        ),
        Text(
          value,
          style: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

