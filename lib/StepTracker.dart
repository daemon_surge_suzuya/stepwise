import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart'; // Added

class StepTracker extends StatelessWidget {
  final int stepsDone;
  final int goal;
  final Function(int) setGoal;

  const StepTracker({Key? key, required this.stepsDone, required this.goal, required this.setGoal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double progress = (stepsDone / goal).clamp(0.0, 1.0);

    return Expanded(
      child: Container(
        constraints: const BoxConstraints(maxHeight: 200.0),
        child: FractionallySizedBox(
          widthFactor: 0.9,
          heightFactor: 0.90,
          child: CircularPercentIndicator( // Fixed
            radius: 100.0,
            lineWidth: 11.0,
            percent: progress,
            circularStrokeCap: CircularStrokeCap.round,
            center: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.directions_walk,
                  size: 34,
                  color: Color(0xFFD0EBFF),
                ),
                const SizedBox(height: 10),
                Text(
                  '$stepsDone',
                  style: const TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _showGoalDialog(context);
                  },
                  child: Text(
                    'OF $goal',
                    style: const TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        decoration: TextDecoration.overline),
                  ),
                ),
              ],
            ),
            progressColor: Colors.blue,
            animation: true,
            backgroundColor: Colors.white,
          ),
        ),
      ),
    );
  }

  Future<void> _showGoalDialog(BuildContext context) async {
    TextEditingController controller = TextEditingController();
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Set Goal'),
          content: TextField(
            controller: controller,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(labelText: 'Enter new goal'),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                int newGoal = int.tryParse(controller.text) ?? goal;
                // Validate newGoal if needed
                setGoal(newGoal);
                Navigator.pop(context);
              },
              child: const Text('Set'),
            ),
          ],
        );
      },
    );
  }
}

